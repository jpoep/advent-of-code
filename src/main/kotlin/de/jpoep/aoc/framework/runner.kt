package de.jpoep.aoc.framework

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.*
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.httpGet
import org.reflections.Reflections
import java.io.File
import java.time.LocalDateTime

const val YEARS_PACKAGE = "de.jpoep.aoc.years"
const val SESSION_COOKIE =
    "hidden until I implement proper authentication"

fun main(args: Array<String>) = Aoc().main(args)

class Aoc : CliktCommand() {
    private val puzzle by option(
        help = "The puzzle to solve. Format should be <year>/<day>/<part>, e.g. 2020/1/1. Defaults to the current date.",
        metavar = "PUZZLE-ID",
    )
        .convert { PuzzleId from it }
        .defaultLazy {
            with(LocalDateTime.now()) {
                PuzzleId(year, dayOfMonth, 1)
            }
        }

    private val testInput by option(
        help = "Pass if you want to use test input rather than real input. " +
                "Requires that test input is provided for the given puzzle."
    )
        .flag()

    override fun run() {
        with(Reflections(YEARS_PACKAGE)) {
            getSubTypesOf(Puzzle::class.java)
        }.find { clazz ->
            clazz.packageName.split('.')
                .run { subList(size - 2, size) }
                .map { it.toInt() }
                .let { it[0] == puzzle.year && it[1] == puzzle.day }
        }?.kotlin?.constructors?.first()?.call(resolveInput(puzzle, testInput))
            ?.let { puzzleSolver ->
                when (puzzle.part) {
                    1 -> puzzleSolver.part1
                    2 -> puzzleSolver.part2
                    else -> null
                }?.let {
                    println("Answer for ${puzzle.year}/${puzzle.day}/${puzzle.part}: $it")
                }
            }
            ?: println(
                "The puzzle you requested either isn't implemented yet or the date you entered is out of range. " +
                        "Please try again with another one!"
            )
    }
}

fun resolveInput(puzzleId: PuzzleId, testInput: Boolean = false): File {
    val filePath = "${System.getProperty("user.dir")}/src/main/resources/${puzzleId.year}/${puzzleId.paddedDay}"
    return File(filePath, "input.txt").also { file ->
        if (!file.exists()) {
            puzzleId
                .inputUrl
                .httpGet()
                .header(Headers.COOKIE, SESSION_COOKIE)
                .responseString().third.get()
                .let { puzzleInput ->
                    file.parentFile.mkdirs()
                    file.writeText(puzzleInput)
                }
        }
    }
}

data class PuzzleId(val year: Int, val day: Int, val part: Int? = null) {
    val paddedDay
        get() = day.toString().padStart(2, '0')

    val inputUrl
        get() = "https://adventofcode.com/$year/day/$day/input"

    companion object {
        infix fun from(str: String): PuzzleId =
            str.takeIf { "^\\d{4}\\/\\d{1,2}\\/\\d{1,2}\$".toRegex().matches(it) }
                ?.split("/")
                ?.map { it.toInt() }
                ?.let { PuzzleId(it[0], it[1], it[2]) }
                ?: throw IllegalArgumentException("Value must be in format <year>/<day>/<part>, e.g. 2020/1/1")
    }

}

abstract class Puzzle(protected val input: File) {
    protected val lines: List<String> = input.readLines()
    protected val text: String = input.readText()

    abstract val part1: Long
    abstract val part2: Long
}
