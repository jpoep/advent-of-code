package de.jpoep.aoc.years.`2020`.`09`

import de.jpoep.aoc.framework.Puzzle
import de.jpoep.aoc.framework.PuzzleId
import java.io.File

class Day09(input: File) : Puzzle(input) {
    private val longInput = lines.map { it.toLong() }

    override val part1: Long
        get() {
            for (i in longInput.indices) {
                if (i >= 25)
                    if (!validateNumber(longInput[i], longInput.subList(i - 25, i))) {
                        return longInput[i]
                    }
            }
            throw Exception("Wrong input.")
        }
    override val part2: Long
        get() {
            val sum = part1
            for (i in longInput.indices) {
                var attemptList = listOf<Long>()
                while (attemptList.sum() < sum) {
                    attemptList += longInput[i + attemptList.size]
                }
                if (attemptList.sum() == sum) {
                    return attemptList.minOrNull()!! + attemptList.maxOrNull()!!
                }
            }
            throw Exception("Wrong input.")
        }

    fun validateNumber(number: Long, validationList: List<Long>): Boolean =
        validationList.any { validationNumber ->
            validationList
                .filter { it != validationNumber }
                .any { it + validationNumber == number }
        }

}

