package de.jpoep.aoc.years.`2020`.`12`

import de.jpoep.aoc.framework.Puzzle
import java.io.File
import java.lang.Math.abs

fun fromValue(number: Int): Day12.Orientation =
    when (number) {
        0 -> Day12.Orientation.NORTH
        1 -> Day12.Orientation.EAST
        2 -> Day12.Orientation.SOUTH
        3 -> Day12.Orientation.WEST
        else -> throw Exception("Wrong number")
    }

fun fromChar(char: Char): Day12.Orientation =
    when (char) {
        'N' -> Day12.Orientation.NORTH
        'E' -> Day12.Orientation.EAST
        'S' -> Day12.Orientation.SOUTH
        'W' -> Day12.Orientation.WEST
        else -> throw Exception("Wrong number")
    }

class Day12(input: File) : Puzzle(input) {

    override val part1: Long
        get() = Ship().also { ship ->
            lines.forEach {
                val number = it.substring(1).toInt()
                when (it[0]) {
                    in listOf('L', 'R') -> ship.turn(Direction.valueOf(it[0].toString()), number)
                    in listOf('N', 'E', 'S', 'W') -> ship.move(fromChar(it[0]), number)
                    'F' -> ship.forward(number)
                }
            }
        }.manhattanDistanceToStart.toLong()
    override val part2: Long
        get() = 0

    data class ShipWithWaypoint(var wpPosition: Pair<Int, Int> = 10 to 1, var shipPosition: Pair<Int, Int> = 0 to 0) {
        fun turn(direction: Direction, angle: Int) {

        }

        fun moveShip(distance: Int) {
            this.shipPosition = this.shipPosition.first + this.wpPosition.first to this.shipPosition.second to this.wpPosition.second
        }

        fun moveWaypoint(orientation: Orientation, distance: Int) {
            this.wpPosition = this.wpPosition.first + orientation.xDir * this.wpPosition.second + distance to orientation.yDir
        }
    }

    data class Ship(var orientation: Orientation = Orientation.EAST, var position: Pair<Int, Int> = 0 to 0) {
        fun turn(direction: Direction, angle: Int) {
            val turnValue: Int = angle / 90 * direction.factor
            val newOrientationValue = (4 + orientation.number + turnValue) % 4
            orientation = fromValue(newOrientationValue)
        }

        fun forward(distance: Int) {
            this.position =
                this.position.first + this.orientation.xDir * distance to this.position.second + this.orientation.yDir
        }

        fun move(orientation: Orientation, distance: Int) {
            this.position = this.position.first + orientation.xDir * this.position.second + distance to orientation.yDir
        }

        val manhattanDistanceToStart: Int
            get() = kotlin.math.abs(this.position.first + this.position.second)
    }

    enum class Orientation(val number: Int) {
        NORTH(0), EAST(1), SOUTH(2), WEST(3);

        val xDir
            get() = when (this) {
                EAST -> 1
                WEST -> -1
                else -> 0
            }
        val yDir
            get() = when (this) {
                NORTH -> 1
                SOUTH -> -1
                else -> 0
            }
    }

    enum class Direction(val factor: Int) {
        L(-1), R(1);
    }

}