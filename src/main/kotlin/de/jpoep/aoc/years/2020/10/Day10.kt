package de.jpoep.aoc.years.`2020`.`10`

import de.jpoep.aoc.framework.Puzzle
import java.io.File
import kotlin.math.pow

class Day10(input: File) : Puzzle(input) {
    private val intLines = lines.map { it.toInt() }

    override val part1: Long
        get() = chunkList.run {
            count { it == 1 } * count { it == 3 }
        }.toLong()

    override val part2: Long
        get() = chunkList
            .joinToString("")
            .split("3")
            .filter { it.isNotBlank() }
            .groupingBy { it.length }
            .eachCount()
            .map { (chunkLength, count) ->
                lazyCaterersSequence(chunkLength.toDouble() - 1).pow(count)
            }
            .reduce { acc, d -> acc * d }.toLong()

    private fun lazyCaterersSequence(input: Double) = 1 + (input * (input + 1)) / 2

    private val chunkList: List<Int>
        get() =
            intLines
                .let { it + (it.maxOrNull()!!.plus(3)) }
                .plus(0)
                .sorted()
                .zipWithNext { a, b -> b - a }
}


