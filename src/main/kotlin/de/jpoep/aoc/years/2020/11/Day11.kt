package de.jpoep.aoc.years.`2020`.`11`

import de.jpoep.aoc.framework.Puzzle
import java.io.File
import java.lang.Exception

typealias XPos = Int
typealias YPos = Int

class Day11(input: File) : Puzzle(input) {

    override val part1: Long
        get() {
            var lastAmountOfSeats: Int
            do {
                lastAmountOfSeats = board.numSeated
                board = board.nextIteration()
            } while (board.numSeated != lastAmountOfSeats)
            return board.numSeated.toLong()
        }
    override val part2: Long
        get() {
            var lastAmountOfSeats: Int
            do {
                lastAmountOfSeats = board.numSeated
                board = board.nextIteration2()
            } while (board.numSeated != lastAmountOfSeats)
            return board.numSeated.toLong()
        }

    var board =
        Board(
            Grid(lines.map { line ->
                Row(line.map {
                    Cell(
                        when (it) {
                            '#' -> CellState.OCCUPIED
                            '.' -> CellState.SPACE
                            'L' -> CellState.EMPTY
                            else -> throw Exception("kek")
                        }
                    )
                })
            })
        )

    class Board(val board: Grid) {
        val numSeated
            get() = board.rows.fold(0) { acc, row ->
                acc + row.cells.count { it.state == CellState.OCCUPIED }
            }


        fun cellAt(x: XPos, y: YPos) =
            board.rowAt(y)?.cellAt(x)

        fun getNeighbors(x: XPos, y: YPos): Int =
            listOf(
                -1 to -1, -1 to 1, 1 to -1, 1 to 1,
                -1 to 0, 1 to 0, 0 to -1, 0 to 1
            )
                .map { it.first + x to it.second + y }
                .mapNotNull { cellAt(it.first, it.second) }
                .count { it.state == CellState.OCCUPIED }

        fun getVisibleNeighbors(x: XPos, y: YPos): Int =
            listOf(
                -1 to -1, -1 to 1, 1 to -1, 1 to 1,
                -1 to 0, 1 to 0, 0 to -1, 0 to 1
            )
                .mapNotNull { getNextSeat(x, y, it) }
                .count { it.state == CellState.OCCUPIED }

        fun getNextSeat(x: XPos, y: YPos, direction: Pair<Int, Int>): Cell? {
            var nextSeat: CellState?
            var counter = 1
            do {
                nextSeat = cellAt(x + direction.first * counter, y + direction.second * counter)?.state
                counter++
            } while (nextSeat == CellState.SPACE)
            return nextSeat?.let { Cell(nextSeat) }
        }

        fun nextIteration(): Board =
            Board(Grid(board.rows.mapIndexed { y, row ->
                Row(row.cells.mapIndexed { x, cell ->
                    cell.transform(getNeighbors(x, y))
                })
            }))

        fun nextIteration2(): Board =
            Board(Grid(board.rows.mapIndexed { y, row ->
                Row(row.cells.mapIndexed { x, cell ->
                    cell.transform2(getVisibleNeighbors(x, y))
                })
            }))

        override fun toString(): String =
            board.rows.joinToString("\n") { it.cells.joinToString(" ") { it.toString() } }
    }

    data class Grid(val rows: List<Row> = listOf()) {
        fun rowAt(y: YPos) = y.takeIf { y >= 0 && y <= rows.lastIndex }?.let { rows[y] }
    }

    data class Row(val cells: List<Cell> = listOf()) {
        fun cellAt(x: XPos) = x.takeIf { x >= 0 && x <= cells.lastIndex }?.let { cells[x] }
    }

    class Cell(state: CellState) {
        var state = state
            private set

        fun transform(neighborCount: Int): Cell =
            state.takeIf { it == CellState.SPACE }?.let { Cell(state) }
                ?: when (neighborCount) {
                    in (4..9) -> Cell(CellState.EMPTY)
                    0 -> Cell(CellState.OCCUPIED)
                    else -> Cell(state)
                }

        fun transform2(neighborCount: Int): Cell =
            state.takeIf { it == CellState.SPACE }?.let { Cell(state) }
                ?: when (neighborCount) {
                    in (5..9) -> Cell(CellState.EMPTY)
                    0 -> Cell(CellState.OCCUPIED)
                    else -> Cell(state)
                }

        override fun toString(): String =
            when (state) {
                CellState.OCCUPIED -> "#"
                CellState.EMPTY -> "L"
                CellState.SPACE -> "."
            }
    }

    enum class CellState {
        OCCUPIED, EMPTY, SPACE
    }

}