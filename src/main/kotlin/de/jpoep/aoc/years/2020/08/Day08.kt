package de.jpoep.aoc.years.`2020`.`08`

import de.jpoep.aoc.framework.Puzzle
import de.jpoep.aoc.framework.PuzzleId
import java.io.File

class Day08(input: File) : Puzzle(input) {
    override val part1: Long
        get() = Computer(instructions)
            .apply { executeUntilTermination() }
            .run { accumulator.toLong() }
    override val part2: Long
        get() {
            getInstructionPermutations(instructions).forEach {
                Computer(it).apply {
                    if (executeUntilTermination()) {
                        return accumulator.toLong()
                    }
                }
            }
            throw Exception("Can't solve.")
        }

    private val instructions =
        lines.map {
            it.split(" ").let { instruction ->
                Computer.Instruction.valueOf(instruction[0].toUpperCase()) to instruction[1].toInt()
            }
        }

    private fun getInstructionPermutations(instructions: List<Pair<Computer.Instruction, Int>>): List<List<Pair<Computer.Instruction, Int>>> {
        val returnList: MutableList<List<Pair<Computer.Instruction, Int>>> = mutableListOf()
        for (i in instructions.indices) {
            returnList += instructions.mapIndexed { index, pair ->
                if (index == i) pair.first.flipped to pair.second
                else pair
            }
        }
        return returnList.distinct()
    }

    class Computer(private val instructions: List<Pair<Instruction, Int>>) {
        var accumulator: Int = 0
            private set

        private var instructionPointer: Int = 0
        private val instructionMap: Map<Instruction, (Int) -> Int> =
            mapOf(
                Instruction.ACC to { instructionValue ->
                    1.also { accumulator += instructionValue }
                },
                Instruction.NOP to {
                    1
                },
                Instruction.JMP to {
                    it
                })

        private val executionTracker: MutableMap<Int, Boolean> = mutableMapOf()

        fun executeUntilTermination(): Boolean {
            while (executionTracker[instructionPointer] != true && instructions.elementAtOrNull(instructionPointer) != null) {
                val instruction = instructions[instructionPointer]
                executionTracker[instructionPointer] = true
                instructionPointer += instructionMap[instruction.first]!!(instruction.second)
            }
            return instructions.elementAtOrNull(instructionPointer) == null
        }

        enum class Instruction {
            NOP, ACC, JMP;

            val flipped: Instruction
                get() = when (this) {
                    NOP -> JMP
                    JMP -> NOP
                    ACC -> ACC
                }
        }
    }
}

